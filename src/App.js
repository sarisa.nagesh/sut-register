
import React, { Component } from 'react'; 
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'; 

import register from './pages/register';

import './App.css'; 


class App extends Component { 
render() { 
	return ( 
	<Router> 
			<Switch> 

			<Route exact path='/' component={register}></Route>
			
			</Switch> 
		
	</Router> 
); 
} 
} 

export default App; 
