import React, { Component } from "react";
import "./register.css";
import packageimage from "../images/packageimage.png";
import logo1 from "../images/logo1.png";
import { BrowserRouter as Link } from "react-router-dom";
class register extends Component {
  render() {
    return (
      <>
        <div className="one-section pt-5">
          <div className="d-flex flex-row mr-auto">
            {/* <div class="container">
        <div class="row"> */}
            <div className="col-sm-12 col-lg-4">
              <img className="pckimg" src={packageimage} alt="" />
            </div>
            <div className="col-sm-12 col-lg-8">
              <div className="d-flex flex-row">
                <div className="HireForAnyScope-card">
                  <h3 className="head1 p-2">Want to send</h3>
                  <h3 className="head1 p-2">(or) Recieve package</h3>
                  <h2 className="head2 pt-5 pl-5">Register</h2>
                  <h5 className="head2 pt-2 pl-5">
                    Already Registered? Please
                    <Link className="login" href>
                      Login
                    </Link>
                  </h5>
                  <div className="d-flex flex-column">
                    <input
                      className="inputbox1 mt-3 ml-5"
                      type="text"
                      placeholder="First Name"
                      name="name"
                    />
                    <input
                      className="inputbox1 mt-3 ml-5"
                      type="text"
                      placeholder="Last Name"
                      name="name"
                    />
                    <input
                      className="inputbox1 mt-3 mb-3 ml-5"
                      type="tel"
                      id="phone"
                      name="phone"
                      placeholder="Mobile Number"
                      pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    />
                  </div>
                  <div className="ml-5">
                    <div className="ml-5">
                      <div className="ml-5">
                        <div className="ml-5">
                          <div className="ml-5">
                            <button className="next">
                              <i
                                className="fas fa-arrow-right mt-5 ml-5"
                                id="icon1"
                              />
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mt-5">
                  <div className="mt-5 pt-5">
                    <img className="logoimg mt-5" src={logo1} alt="" />
                  </div>
                </div>
              </div>
            </div>
            {/* </div>
        </div> */}
          </div>
        </div>
      </>
    );
  }
}
export default register;
